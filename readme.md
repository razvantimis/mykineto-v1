## My Kineto
Vine in sprijinul pacientilor, dandule posibilitatea de as urma planul de recuperare de acasa

# Dev

- instalati docker
- faceti clona si intrati cu linia de comanda in director
- rulati comanda docker compose build
- rulati comanda docker compose up
- dupa intrati in admin de la keycloak: http://192.168.99.100:8089/auth/ cu admin admin
dati import la realm-export.json

# Tests
 - pentru a rula testele, mergeti in web-client/tests
 - node <fisierul de test>.js
 - trebuie instalat global urmatoarea librarie
 - sau daca nu va merge deloc sudo npm install -g chromedriver --unsafe-perm=true --allow-root