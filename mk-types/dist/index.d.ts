export interface Query {
    getPatients?: BundlePatient | null;
}
export interface BundlePatient {
    total?: number | null;
    entry?: Patient[] | null;
}
export interface Patient {
    id: string;
    active?: boolean | null;
    name: HumanName;
    email: string;
    phone: string;
    gender?: Gender | null;
    program?: Program[] | null;
}
export interface HumanName {
    family?: string | null;
    given?: string | null;
}
export interface Program {
    id?: string | null;
    name?: string | null;
    exercises?: Exercise[] | null;
}
export interface Exercise {
    id?: string | null;
    type?: ExerciseType | null;
    description?: string | null;
    name?: string | null;
    orderMedia?: Media[] | null;
}
export interface Media {
    name?: string | null;
    publicUrl?: string | null;
    description?: string | null;
}
export interface Mutation {
    saveExercise?: Exercise | null;
    savePatient?: Patient | null;
}
export interface LifeStyle {
    id?: string | null;
    weight?: number | null;
    height?: number | null;
    sleepHours?: number | null;
}
export interface ExerciseInput {
    id?: string | null;
    type?: ExerciseType | null;
    description?: string | null;
    name?: string | null;
    orderMedia?: MediaInput[] | null;
}
export interface MediaInput {
    name?: string | null;
    publicUrl?: string | null;
    description?: string | null;
}
export interface PatientInput {
    id?: string | null;
    active?: boolean | null;
    name: HumanNameInput;
    email: string;
    phone: string;
    gender?: Gender | null;
}
export interface HumanNameInput {
    family?: string | null;
    given?: string | null;
}
export interface GetPatientsQueryArgs {
    pageSize?: number | null;
    pageIndex?: number | null;
    filterValue?: string | null;
}
export interface SaveExerciseMutationArgs {
    exercise?: ExerciseInput | null;
}
export interface SavePatientMutationArgs {
    patient?: PatientInput | null;
}
export declare type Gender = "male" | "female";
export declare type ExerciseType = "TIME" | "REP_BASE";
export declare type FrequencyEnum = "EVERYWEEK" | "DAY";
