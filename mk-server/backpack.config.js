const path = require('path');
module.exports = {
  webpack: (config, options, webpack) => {
    config.entry.main = [
      './main.ts'
    ];
    config.devServer = {
      host: '0.0.0.0',
      port: 3000
    };
    config.resolve = {
      extensions: [".ts", ".js", ".json"],
      alias: {
        "mk-qraphql": path.resolve(__dirname, 'app/graphql/'),
        "mk-repository": path.resolve(__dirname, 'app/repository/'),
        "mk-utils": path.resolve(__dirname, 'app/utils/'),
        "mk-routes": path.resolve(__dirname, 'app/routes/')
      }
    };

    config.module.rules.push({
      test: /\.ts$/,
      exclude: ['node_modules'],
      loader: 'awesome-typescript-loader'
    });
    config.plugins.splice(1, 1)

    return config
  }
}

