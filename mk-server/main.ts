import {MyKinetoServer} from "./app/index";

const config = {
  port: 3000
};
async function start() {
  try {
      let server: MyKinetoServer = new MyKinetoServer(config);
      await server.start();
      return server;
  } catch(error) {
      console.log('Cannot start app', error);
  }
}
let server: any;
(async () => server = await start())();