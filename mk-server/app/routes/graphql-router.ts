import * as Router from "koa-router";
import {buildGraphIQLRouteHandler, buildGraphQLRouteHandler, schema} from "../graphql";
import {graphql, introspectionQuery} from "graphql";

export class GraphqlRouter extends Router {

    constructor() {
        super();

        this.post('/graphql', buildGraphQLRouteHandler());
        this.get('/graphql', buildGraphIQLRouteHandler());
        this.get('/graphql/schema', async(ctx: any) => {
            ctx.body = await graphql(schema, introspectionQuery)
        });

    }


}