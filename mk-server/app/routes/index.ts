import * as Router from "koa-router";
import {GraphqlRouter} from "./graphql-router";
import {FileUploadRouter} from "./fileupload-router";

export class PrivateRouter extends Router {

    constructor() {
        super();

        const graphqlRouter: GraphqlRouter = new GraphqlRouter();
        this.use(graphqlRouter.routes()).use(graphqlRouter.allowedMethods());
        const fileUploadRouter: FileUploadRouter = new FileUploadRouter();
        this.use(fileUploadRouter.routes()).use(fileUploadRouter.allowedMethods());

    }


}