import * as Router from "koa-router";
import multer = require("koa-multer");
import * as storage from "@google-cloud/storage";
import * as fs from "fs";

export class FileUploadRouter extends Router {

    constructor() {
        super();

        const GCLOUD_STORAGE_BUCKET = 'mykineto-media';
        const GCLOUD_KEY_FILE = './app/config/mykineto-cloud-storage.json';

        // Instantiate a storage client
        const googleCloudStorage = storage({
            keyFilename: GCLOUD_KEY_FILE
        });

        const upload = multer({
            storage: multer.memoryStorage(),
            limits: {
                fileSize: 5 * 1024 * 1024 // no larger than 5mb
            }
        });

        // A bucket is a container for objects (files).
        const bucket = googleCloudStorage.bucket(GCLOUD_STORAGE_BUCKET);

        // Process the file upload and upload to Google Cloud Storage.
        this.post("/fileUpload", upload.single('file'), async (ctx, next) => {

            const {file} = <any>ctx.req;
            if (!file) {
                ctx.throw(400, "No file uploaded.");
            }
            // Create a new blob in the bucket and upload the file data.
            const blob = bucket.file(file.originalname);

            await new Promise((resolve, reject) => {
                // Make sure to set the contentType metadata for the browser to be able
                // to render the image instead of downloading the file (default behavior)

                const writeStream = blob.createWriteStream({
                    metadata: {
                        contentType: file.mimetype
                    }
                });

                writeStream.on("error", err => {
                    reject(err)
                });

                writeStream.on("finish", () => {
                    blob
                        .makePublic()
                        .then(() => {
                            const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
                            ctx.status = 200;
                            ctx.body = {
                                name: blob.name,
                                publicUrl: publicUrl
                            };
                            resolve();
                        }).catch((err: any) => {
                        reject(err);
                    })
                });

                writeStream.end(file.buffer)
            }).catch((err) => ctx.throw(500, err.message))


        });
    }


}