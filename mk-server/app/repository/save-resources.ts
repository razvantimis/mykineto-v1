import {TimingLogger} from "mk-utils/timer-logger";
import {mkRepoMongoose} from "mk-repository/resource";

/**
 * Save or update resource
 * @param type what name have resource, like Patient, Media
 * @param resource this data is save in mongo
 * @return saved resource
 */
export async function saveResource(type: string, resource: any): Promise<any> {
    let timingLogger = new TimingLogger(`Save/update resources`);
    let model = new (mkRepoMongoose.model(type))(resource);
    await model.save();
    return model;
}