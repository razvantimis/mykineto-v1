import {MediaSchema} from './MediaSchema';
import {Schema} from 'mongoose'


export const ExerciseSchema = new Schema({
  name: String,
  type: { type: String, enum: ["TIME", "REP_BASE"] },
  description: String,
  orderMedia: {
    type: [MediaSchema], default: void 0
  }
});