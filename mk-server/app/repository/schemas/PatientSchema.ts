import {Schema, Types} from 'mongoose'

const HumanNameSchema: any = new Schema({
  family: String,
  given: {
    type: [String],
    default: void 0
  },
}, {_id: false});

export const PatientSchema = new Schema({
  name: {
    type: [HumanNameSchema], default: void 0
  },
  email: String,
  phone: String,
  gender: { type: String, enum: ["male", "female"] },
});