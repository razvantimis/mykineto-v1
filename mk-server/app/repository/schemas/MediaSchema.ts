import {Schema, Types} from 'mongoose'

export const MediaSchema = new Schema({
  name: String,
  publicUrl: String,
  desciption: String,
});