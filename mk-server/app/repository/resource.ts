import * as mongoose from 'mongoose'
//====================Dynamic Module Loading from repository========================
let importedModels: string[] = [];
let tryImportFromDirs: string[] = [''];

interface RepositoryConfig {
    model: Function
}

export const mkRepoMongoose: RepositoryConfig = {
    model: (name: string) => {
        try {
            let errors = [];

            if (importedModels.indexOf(name) === -1) {
                let modelWasFound = false;
                //=============================================== TRY import model =====================================
                for (let i = 0; i < tryImportFromDirs.length; i++) {
                    let dir = tryImportFromDirs[i];
                    try {
                        require(`./${dir}models/${name}Model`);
                        importedModels.push(name);
                        modelWasFound = true;
                        break;
                    } catch (error) {
                        errors.push(error);
                    }
                }
                //======================================================================================================
                if (!modelWasFound) {
                    throw new Error(`Resource ${name}Schema not found in mk-server!\n` + errors.map((err) => `${err.message}\n${err.stack}`));
                }
            }

            return mongoose.model(name);
        } catch (error) {
            throw `Unknown resource collection ${name}\n${error.message}\n${error.stack}`;
        }
    }
};
//==================================================================================