import {model} from 'mongoose';
import {MediaSchema, Schema} from 'mk-repository/schemas' 

const MediaModel = model(Schema.Media, MediaSchema);
export default MediaModel;