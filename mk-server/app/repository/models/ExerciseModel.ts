import {model} from 'mongoose';
import {ExerciseSchema, Schema} from 'mk-repository/schemas' 

const ExerciseModel = model(Schema.Exercise, ExerciseSchema);
export default ExerciseModel;