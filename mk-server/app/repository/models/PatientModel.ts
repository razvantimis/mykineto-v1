import {model} from 'mongoose';
import {PatientSchema, Schema} from 'mk-repository/schemas' 

const PatientModel = model(Schema.Patient, PatientSchema);
export default PatientModel;