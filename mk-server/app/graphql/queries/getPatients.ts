
let dataPatient = [
    {
        id: '23',
        active: true,
        name: {
            family: 'Timis',
            given: 'Razvan Vasile'
        },
        email: 'timisrazvanvasile@gmail.com',
        phone: '0746339170',
        gender: 'male',
        cominication: [
            {
                language: 'ro',
                prefered: true
            }
        ],
        anamnesis: {
            id: '1',
            idPatient: '23',
            lifeStyle: {
                id: '2',
                weight: 70,
                height: 182,
                sleepHours: 7
            }
        },
        program: [
            {
                id: '3',
                name: 'primul',
                frequency: {
                    number: 3,
                    type: 'EVERYWEEK'
                },
                exercises: [
                    {
                        id: '3',
                        type: ['STRENGTH'],
                        bodyPart: ['CORE'],
                        media: [
                            {
                                id: '245',
                                type: 'PNG',
                                file: '../file/image1.png',
                            }
                        ],
                    }
                ]
            }
        ]
    },
    {
        id: '25',
        active: true,
        name: {
            family: 'Mihai',
            given: 'Alin'
        },
        email: 'mihail@gmail.com',
        phone: '07463345170',
        gender: 'male'
    },
    {
        id: '26',
        active: true,
        name: {
            family: 'Tudose',
            given: 'Alex'
        },
        email: 'tudose@gmail.com',
        phone: '07467839170',
        gender: 'male'
    },
    {
        id: '27',
        active: true,
        name: {
            family: 'Tomus',
            given: 'Madalina'
        },
        email: 'tomus.madalina@gmail.com',
        phone: '07467836770',
        gender: 'female'
    },
    {
        id: '28',
        active: true,
        name: {
            family: 'Valican',
            given: 'Cristina'
        },
        email: 'cristina98@gmail.com',
        phone: '07467839670',
        gender: 'female'
    }
];
let i = 0;
while (i < 10) {
    dataPatient = dataPatient.concat(dataPatient)
    dataPatient = dataPatient.concat(dataPatient)
    i++;
}


export default async (_: any, {pageIndex, pageSize, filterValue}: any) => {
    console.log(pageIndex, pageSize, filterValue);
    const data = dataPatient
        .filter((item) => !filterValue || filterValue === '' || item.name.given.toLowerCase().indexOf(filterValue) !== -1 || item.name.family.toLowerCase().indexOf(filterValue) !== -1);

    return {
        total: data.length,
        entry: data.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize)
    }
}