import {join} from 'path'
import {readFileSync} from 'fs'
import {makeExecutableSchema} from 'graphql-tools'
import {graphiqlKoa, graphqlKoa} from 'apollo-server-koa';

import mutations from './mutations'
import queries from './queries'


export const schemaFile = readFileSync(join(__dirname, './schema.graphql')).toString();

export const resolvers = {
    Query: queries,
    Mutation: mutations,
};
export const schema = makeExecutableSchema({
    typeDefs: schemaFile,
    resolvers
});

export function buildGraphQLRouteHandler() {
    return graphqlKoa((ctx: any) => ({
        schema,
        context: ctx,
    }))
}

export function buildGraphIQLRouteHandler() {
    return graphiqlKoa({endpointURL: '/graphql'})
}