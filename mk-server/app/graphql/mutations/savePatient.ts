import {saveResource} from 'mk-repository/save-resources';
import {Schema} from "mk-repository/schemas";

export default async function (object: any, {patient}: {patient: any}, request: Request) {
    return saveResource(Schema.Patient, patient);
}