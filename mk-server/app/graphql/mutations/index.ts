import saveExercise from "./saveExercise";
import savePatient from "./savePatient";


export default {
    saveExercise,
    savePatient
}