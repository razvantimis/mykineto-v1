import {ExerciseInput} from 'mk-types';
import {saveResource} from 'mk-repository/save-resources';
import {Schema} from 'mk-repository/schemas';

export default async function (object: any, {exercise}: {exercise: ExerciseInput}, request: Request) {
    const exerciseSave = await saveResource(Schema.Exercise, exercise);;
    console.log(exerciseSave);
     
    return exerciseSave;
}