import {Server} from "http";
import {errorHandler} from "./utils/error-handler";
import {PrivateRouter} from "./routes";
import * as koaBody from "koa-bodyparser";
import * as mongoose from "mongoose";


const Koa: any = require("koa");
const cors: any = require("koa-cors");
const convert: any = require("koa-convert");

export class MyKinetoServer {
    private app: any;
    public listeningHttpServer: Server;
    private config: any;

    constructor(config: any) {
        this.config = config;
        console.log("Create KOA Server");
        this.app = new Koa();

        this.configMiddlewares();
        this.configRoutes();
    }

    private configMiddlewares() {
        console.log("Config KOA Server Middlewares");
        this.app.use(errorHandler());
        this.app.use(koaBody());
        this.app.use(convert(cors()));


    }

    private configRoutes() {
        const privateRouter = new PrivateRouter();
        this.app
            .use(privateRouter.routes())
            .use(privateRouter.allowedMethods());
    }

    async start() {
        this.listeningHttpServer = this.app.listen(
            this.config.port,
            (error: any) => {
                if (error) {
                    console.log("Error while trying to open server.", error);
                    return;
                }
                console.log("Server started on port", this.config.port);
                mongoose.connect("mongodb://mongo:27017/mykineto");
            }
        );

        return this.listeningHttpServer;
    }
}
