
const winston = require('winston');
require('winston-daily-rotate-file');
const stringify = require('json-stringify-safe');


let config: any = require("winston/lib/winston/config");
function timestamp() {
    return (new Date()).toISOString();
}

function baseFormatter(options: any, level: string) {
    return options.timestamp() + ' '
        + level + ' '
        + (options.message ? options.message : '')
        + (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
}

function colorLoggerFormatter(options: any) {
    // { error: 'red', warn: 'yellow', help: 'cyan', data: 'grey', info: 'green', debug: 'blue', prompt: 'grey', verbose: 'cyan', input: 'grey', silly: 'magenta', emerg: 'red', alert: 'yellow', crit: 'red', warning: 'red', notice: 'yellow' }

    let level: string = options.level.toUpperCase();
    // let _tagName: string = tagName;
    if (options.colorize === true) {
        level = config.colorize(options.level, level);
        // _tagName = config.colorize('verbose', _tagName);
    }

    return baseFormatter(options, level);
}


const logger: any = new winston.Logger(<any>{
    level: 'silly',
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            timestamp: timestamp,
            formatter: colorLoggerFormatter,
            prettyPrint: true,
            stderrLevels: ['error']
        }),
        // new (winston.transports.File)({
        //     filename: 'somefile.log',
        //     timestamp: timestamp,
        //     formatter: formatter
        // }),
        new winston.transports.DailyRotateFile({
            colorize: false,
            maxFiles: 30,
            dirname: './logs',
            filename: 'log',
            datePattern: 'yyyy-MM-dd.',
            prepend: true,  //Defines if the rolling time of the log file should be prepended at the beginning of the filename (default 'false').
            // level: process.env.ENV === 'development' ? 'debug' : 'info',
            level: 'debug',
            localTime: false,   // UTC time will be used
            // zippedArchive: true, // don't work well
            timestamp: timestamp,
            formatter: (options: any) => baseFormatter(options, options.level.toUpperCase()),
            stderrLevels: ['error']
        })
    ]
});

function colorizeTagName(tagName: any) {
    return config.colorize('verbose', tagName);
}

export function getLogger(tagName: string) {
    tagName = colorizeTagName(tagName);
    let msgWithTag = (msg: any, ) => {
        if (msg instanceof Object)
            msg = stringify(msg, null, 2);
        return `${tagName}: ${msg}`;
    };
    return {
        info: (msg: any, ...args: any[]) => {
            return logger.info(msgWithTag(msg), ...args);
        },
        debug: (msg: any, ...args: any[]) => logger.debug(msgWithTag(msg), ...args),
        warn: (msg: any, ...args: any[]) => logger.warn(msgWithTag(msg), ...args),
        error: (msg: any, ...args: any[]) => {
            msg = msgWithTag(msg);
            if (msg instanceof Error)
                msg.message = msgWithTag(msg.message);
            logger.error(msg, ...args);
        }
    }
}