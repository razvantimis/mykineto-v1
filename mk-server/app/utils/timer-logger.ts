import {getLogger} from "./logger";

export class TimingLogger {
    message:string;
    timeline:[{label:string, time:any}];
    logger: any;

    constructor(message:string) {
        this.message = message;
        this.timeline = [{label: message, time: new Date()}];
        this.logger = getLogger('TimingLogger');
    }

    // Print log timeline from start
    dumpToLog(message:string):void {
        let timingMsg = this.message ? this.message.concat(':') : '';
        if (this.timeline.length === 1) {
            this.logger.debug(`${message || timingMsg} ${+new Date() - this.timeline[0].time} ms`)
        }
        else {
            this.logger.debug(`${timingMsg} begin`);
            for (let i = 0; i < this.timeline.length - 1; i++) {
                this.logger.debug(`${timingMsg}\t${this.timeline[i + 1].time - this.timeline[i].time} ms, ${this.timeline[i + 1].label}`);
            }
            this.logger.debug(`${timingMsg} end,${message ? " (".concat(message).concat(")") : ""} ${+new Date() - this.timeline[0].time} ms`);
        }
    }

    // Add checkpoint
    addSplit(work:string) {
        this.timeline.push({label: work, time: new Date()})
    }
}