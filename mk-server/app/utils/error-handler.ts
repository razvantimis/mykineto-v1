import {TimingLogger} from "./timer-logger";
import {getLogger} from "./logger";

const logger = getLogger('Error Handler');
export function errorHandler() {
    return async(ctx: any, next: any) => {
        try {
            let timingLogger = new TimingLogger('Compute execution time on server');
            logger.info(`Request to server: ${ctx.request.method} ${ctx.request.url}`);
            await next();
            timingLogger.dumpToLog('Finish resolve request');
        } catch (error) {
            logger.error(error.stack, error);

            ctx.status = error.status || 500;

            if(error) {
                ctx.body = {...error, code: ctx.status};
            }

            ctx.app.emit('error', error , ctx); // see https://github.com/koajs/koa/wiki/Error-Handling
        }
    }
}