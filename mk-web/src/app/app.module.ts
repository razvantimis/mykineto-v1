import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from './shared/core/core.module';
import {MatSidenavModule, MatToolbarModule} from '@angular/material';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SplashScreenComponent} from './shared/components/splash-screen/slash-screen.component';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {reducers} from './app.reducers';
import {HttpClientModule} from '@angular/common/http';
import {Apollo, ApolloModule} from 'apollo-angular';
import {HttpLink, HttpLinkModule} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {EffectsModule} from '@ngrx/effects';
import {RouterEffects} from './shared/router/router.effects';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    SplashScreenComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule, // provides HttpClient for HttpLink
    ApolloModule,
    HttpLinkModule,
    StoreModule.forRoot(reducers),
    AppRoutingModule,
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router' // name of reducer key
    }),
    EffectsModule.forRoot([RouterEffects]),
    StoreDevtoolsModule.instrument({maxAge: 50}),
    BrowserAnimationsModule,
    CoreModule,
    MatSidenavModule,
    MatToolbarModule,
    PerfectScrollbarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(apollo: Apollo,
              httpLink: HttpLink) {
    apollo.create({
      // By default, this client will send queries to the
      // `/graphql` endpoint on the same host
      link: httpLink.create({uri: environment.HOST_SERVER_URL + '/graphql'}),
      cache: new InMemoryCache()
    });
  }
}
