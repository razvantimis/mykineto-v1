import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'clinic',  loadChildren: './modules/clinic/modules/clinic.module#ClinicModule' },
  { path: '',   redirectTo: '/clinic', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
