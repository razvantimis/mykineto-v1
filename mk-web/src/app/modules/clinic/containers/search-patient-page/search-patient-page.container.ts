import {Component, ChangeDetectionStrategy, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Store} from '@ngrx/store';

import * as fromPatients from '../../reducers';
import * as collectionActions from '../../actions/collection.action';
import * as patientActions from '../../actions/patient.action';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as RouterActions from '../../../../shared/router/router.actions';

import {merge} from 'rxjs/observable/merge';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {Patient} from 'mk-types';

@Component({
  selector: 'mk-patient-table-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './search-patient-page.container.html',
  styleUrls: ['./search-patient-page.container.scss']
})
export class TablePatientPageContainer implements OnInit, AfterViewInit {
  isLoading$: Observable<Boolean>;
  length$: Observable<number>;

  displayedColumns = ['ID', 'Name', 'Phone', 'Gender', 'Actions'];
  dataSource = new PatientDataSource(this.store);
  pageSize = 10;
  pageIndex = 0;

  filterValue: BehaviorSubject<string> = new BehaviorSubject('');

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private store: Store<fromPatients.State>) {
    this.isLoading$ = store.select(fromPatients.getLoading);
    this.length$ = store.select(fromPatients.getTotalEntry);
  }

  ngOnInit() {
    this.loadPatients();
  }

  ngAfterViewInit() {
    merge(this.sort.sortChange, this.paginator.page, this.filterValue)
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(() => this.loadPatients());
  }

  private loadPatients() {
    this.pageSize = this.paginator.pageSize;
    this.pageIndex = this.paginator.pageIndex;
    this.store.dispatch(new collectionActions.Load({
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      filterValue: this.filterValue.getValue()
    }));
  }

  onView(patient: Patient) {
    console.log('view', patient);
    this.store.dispatch(new patientActions.Select(patient.id));
  }

  onEdit(patient: Patient) {
    this.store.dispatch(new RouterActions.Go({
      path: ['/clinic/patient', patient.id],
      query: { },
      extras: { replaceUrl: false }
    }));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // defaults to lowercase matches
    this.filterValue.next(filterValue);
  }

  onDelete(patient: Patient) {
    console.log('delete', patient);

  }
}

export class PatientDataSource extends MatTableDataSource<any> {
  patients$: BehaviorSubject<Patient[]> = new BehaviorSubject([]);

  constructor(private store: Store<fromPatients.State>) {
    super();
    this.store.select(fromPatients.getPatientCollection).subscribe((data: any) => this.patients$.next(data));
  }

  connect(): BehaviorSubject<Patient[]> {
    return this.patients$;
  }

  disconnect() {
  }
}
