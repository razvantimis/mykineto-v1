import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromPatients from '../../reducers';
import {Patient} from 'mk-types';


@Component({
  selector: 'mk-add-patient-page',
  templateUrl: './add-patient-page.container.html',
  styleUrls: ['./add-patient-page.container.scss']
})
export class AddPatientPageContainer implements OnInit {

  constructor(private store: Store<fromPatients.State>) {
  }

  ngOnInit() {

  }

  onSavePatient(patient: Patient) {
    console.log(patient);
  }

}
