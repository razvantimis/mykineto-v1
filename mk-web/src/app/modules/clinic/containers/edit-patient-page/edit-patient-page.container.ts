import {Component, Input} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromPatients from '../../reducers';
import {Observable} from 'rxjs/Observable';
import {Patient} from 'mk-types';

@Component({
  selector: 'mk-edit-patient-page',
  templateUrl: './edit-patient-page.container.html',
  styleUrls: ['./edit-patient-page.container.scss']
})
export class EditPatientPageContainer {
  @Input() disabled: Boolean = false;
  patient$: Observable<Patient>;

  constructor(private store: Store<fromPatients.State>) {
    this.patient$ = store.select(fromPatients.getSelectedPatient);
  }

  onUpdatePatient(patient: Patient) {
    console.log(patient);
  }
}
