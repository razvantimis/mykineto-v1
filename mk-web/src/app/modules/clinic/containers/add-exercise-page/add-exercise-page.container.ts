import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileUploader} from 'ng2-file-upload';
import * as fromExercise from '../../reducers/exercise.reducer';
import * as ExerciseActions from '../../actions/exercise.action';
import {ExerciseInput, MediaInput} from 'mk-types';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'mk-add-exercise-page',
  templateUrl: './add-exercise-page.container.html',
  styleUrls: ['./add-exercise-page.container.scss']
})
export class AddExercisePageContainer implements OnInit {
  exerciseForm: FormGroup;
  uploaderFiles: FileUploader;
  public hasBaseDropZoneOver: Boolean = false;

  codingExerciseType: any[] = [{code: 'TIME', display: 'Time'}, {code: 'REP_BASE', display: 'rep base'}];
  orderMedia: MediaInput[] = [];


  constructor(private fb: FormBuilder, private store: Store<fromExercise.State>) {
    this.uploaderFiles = new FileUploader({
      url: environment.HOST_SERVER_URL + '/fileUpload',
      autoUpload: true
    });
    this.uploaderFiles.response.subscribe(res => {
      const objRes = JSON.parse(res);
      if (!objRes.code) {
        this.orderMedia.push({
          name: objRes.name,
          publicUrl: objRes.publicUrl,
          description: ''
        });
      }

    });
  }

  ngOnInit() {
    this.exerciseForm = this.fb.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      description: ['', Validators.required],
    });

    this.uploaderFiles.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    };
  }

  public onSaveExercises() {
    const exercises: ExerciseInput = this.mapDataToExercises();

    this.store.dispatch(new ExerciseActions.SaveLoadingExercise(exercises));
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public moveUp(index: number) {
    this.switchItemArray(index - 1, index, this.orderMedia);
  }

  public moveDown(index: number) {
    this.switchItemArray(index + 1, index, this.orderMedia);
  }

  private mapDataToExercises(): ExerciseInput {
    return <ExerciseInput> {
      name: this.exerciseForm.value.name,
      type: this.exerciseForm.value.type,
      description: this.exerciseForm.value.description,
      orderMedia: this.orderMedia
    };
  }

  private switchItemArray(index1: number, index2: number, array: any[]) {
    const aux = array[index1];
    array[index1] = array[index2];
    array[index2] = aux;
  }
}
