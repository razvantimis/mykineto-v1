import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPatientPageContainer } from './view-patient-page.container';

describe('ViewPatientPageContainer', () => {
  let component: ViewPatientPageContainer;
  let fixture: ComponentFixture<ViewPatientPageContainer>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPatientPageContainer ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPatientPageContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
