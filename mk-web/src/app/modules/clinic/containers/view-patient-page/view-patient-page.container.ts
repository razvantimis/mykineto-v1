import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';
import * as fromPatients from '../../reducers';
import * as patientActions from '../../actions/patient.action';
import {map} from 'rxjs/operators';

@Component({
  selector: 'mk-view-patient-page',
  templateUrl: './view-patient-page.container.html',
  styleUrls: ['./view-patient-page.container.scss']
})
export class ViewPatientPageContainer implements OnDestroy {
  actionsSubscription: Subscription;

  constructor(store: Store<fromPatients.State>, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .pipe(map(params => new patientActions.Select(params.id)))
      .subscribe(store);
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
