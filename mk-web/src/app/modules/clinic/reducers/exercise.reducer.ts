import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import * as exerciseActions from '../actions/exercise.action';
import * as collectionActions from '../actions/collection.action';
import {Exercise} from 'mk-types';

export interface State extends EntityState<Exercise> {
}

export const adapter: EntityAdapter<Exercise> = createEntityAdapter<Exercise>({
  selectId: (exercise: Exercise) => exercise.id,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
});

export function reducer(state = initialState,
                        action: exerciseActions.Actions): State {
  switch (action.type) {

    default: {
      return state;
    }
  }
}

