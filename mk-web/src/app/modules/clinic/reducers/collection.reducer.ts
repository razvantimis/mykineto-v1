import * as collection from '../actions/collection.action';

export interface State {
  loaded: boolean;
  loading: boolean;
  pageSize: number;
  pageIndex: number;
  total: number;
  ids: string[];
}

const initialState: State = {
  loaded: false,
  loading: false,
  pageSize: 10,
  pageIndex: 0,
  total: 0,
  ids: [],
};

export function reducer(state = initialState,
                        action: collection.Actions): State {
  switch (action.type) {
    case collection.LOAD: {
      return {
        ...state,
        pageSize: action.payload.pageSize,
        pageIndex: action.payload.pageIndex,
        loading: true,
      };
    }

    case collection.LOAD_SUCCESS: {
      return {
        ...state,
        loaded: true,
        loading: false,
        ids: action.payload.entry.map(item => item.id),
        total: action.payload.total
      };
    }
    case collection.LOAD_FAIL: {
      return state;
    }
    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getIds = (state: State) => state.ids;
export const getTotal = (state: State) => state.total;
