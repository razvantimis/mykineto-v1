
import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromCollection from './collection.reducer';
import * as fromPatient from './patient.reducer';
import * as fromExercise from './exercise.reducer';
import * as fromRoot from '../../../app.reducers';

export interface ClinicState {
  patient: fromPatient.State;
  collection: fromCollection.State;
  exercise: fromExercise.State;
}


export const reducers = {
  patient: fromPatient.reducer,
  exercise: fromExercise.reducer,
  collection: fromCollection.reducer,
};

export interface State extends fromRoot.AppState {
  'clinic': ClinicState;
}


export const getPatientsState = createFeatureSelector<ClinicState>('clinic');


export const getCollectionState = createSelector(
  getPatientsState,
  (state: ClinicState) => state.collection
);

export const getPatientEntities = createSelector(
  getPatientsState,
  state => state.patient
);

export const getCollectionPatientIds = createSelector(
  getCollectionState,
  fromCollection.getIds
);

export const getPatientCollection = createSelector(
  getPatientEntities,
  getCollectionPatientIds,
  ({entities}, ids) => {
    return ids.map(id => {
      return entities[id];
    });
  }
);

export const getLoading = createSelector(
  getCollectionState,
  fromCollection.getLoading
);

export const getTotalEntry = createSelector(
  getCollectionState,
  fromCollection.getTotal
);

export const getSelectedPatientId = createSelector(
  getPatientEntities,
  fromPatient.getSelectedId
);
export const getSelectedPatient = createSelector(
  getPatientEntities,
  getSelectedPatientId,
  ( {entities}, selectedId) => {
    console.log(selectedId);
    console.log(entities);
    return selectedId && entities[selectedId];
  }
);
