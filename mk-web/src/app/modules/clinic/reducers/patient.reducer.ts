import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import * as patientActions from '../actions/patient.action';
import * as collectionActions from '../actions/collection.action';
import {Patient} from 'mk-types';

export interface State extends EntityState<Patient> {
  selectedPatientId: string | null;
}

export const adapter: EntityAdapter<Patient> = createEntityAdapter<Patient>({
  selectId: (patient: Patient) => patient.id,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
  selectedPatientId: null,
});

export function reducer(state = initialState,
                        action: patientActions.Actions | collectionActions.Actions): State {
  switch (action.type) {
    case collectionActions.LOAD_SUCCESS: {
      return {
        ...adapter.addMany(action.payload.entry, state),
        selectedPatientId: state.selectedPatientId,
      };
    }

    case patientActions.LOAD: {
      return {
        ...adapter.addOne(action.payload, state),
        selectedPatientId: state.selectedPatientId,
      };
    }
    case patientActions.SELECT: {
      return {
        ...state,
        selectedPatientId: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getSelectedId = (state: State) => state.selectedPatientId;
