import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/delay';

import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import {ProgramService} from '../services';


@Injectable()
export class ProgramEffects {
  constructor(
    private actions$: Actions,
    private programService: ProgramService
  ) {}


}
