import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/delay';
import {of as ObservarOf} from 'rxjs/observable/of';

import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import {ExerciseService} from '../services';
import * as ExerciseActions from '../actions/exercise.action';


@Injectable()
export class ExerciseEffects {
  constructor(
    private actions$: Actions,
    private exerciseService: ExerciseService
  ) {}

  @Effect() saveExercise = this.actions$
    .ofType(ExerciseActions.SAVE_LOADING)
    .switchMap((action: any) => this.exerciseService.save(action.payload)
      .delay(1000)
      .map(data => ({ type: ExerciseActions.SAVE_SUCCES, payload: data }))
      .catch(error => ObservarOf({ type: ExerciseActions.SAVE_ERROR, payload: error }))
    );
}
