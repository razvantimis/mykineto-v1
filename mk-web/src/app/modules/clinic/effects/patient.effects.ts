import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/delay';
import {of as ObservarOf} from 'rxjs/observable/of';

import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

import { PatientService } from '../services/';
import * as fromCollection from '../actions/collection.action';
import {Action} from '@ngrx/store';

@Injectable()
export class PatientEffects {
  constructor(
    private actions$: Actions,
    private patientService: PatientService
  ) {}

  @Effect() loadPatients = this.actions$
    .ofType(fromCollection.LOAD)
    .switchMap((action: any) => this.patientService.getPatients(action.payload)
      .delay(1000)
      .map(data => ({ type: fromCollection.LOAD_SUCCESS, payload: data }))
      .catch(error => ObservarOf({ type: fromCollection.LOAD_FAIL, payload: error }))
    );

  // @Effect() updateProfile$ = this.actions$
  //   .ofType(ProfileActions.PROFILE_UPDATE_PROFILE)
  //   .map<Action, any>(toPayload)
  //   .switchMap(({ firstName, lastName }) => this.profileService.updateProfile(firstName, lastName)
  //     .map(user => ({ type: ProfileActions.PROFILE_UPDATE_PROFILE_SUCCESS, payload: user }))
  //     .catch(error => Observable.of({ type: ProfileActions.PROFILE_UPDATE_PROFILE_FAIL, payload: error }))
  //   );
}
