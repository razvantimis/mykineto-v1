import {Action} from '@ngrx/store';
import {Patient} from 'mk-types';

export const LOAD = '[Patient] Load';
export const SELECT = '[Patient] Select';

export class Load implements Action {
  readonly type = LOAD;

  constructor(public payload: Patient) {
  }
}

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: string) {
  }
}

export type Actions = Load | Select;
