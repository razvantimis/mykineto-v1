
import { Action } from '@ngrx/store';
import {Patient} from 'mk-types';

export const LOAD = '[Collection] Load';
export const LOAD_SUCCESS = '[Collection] Load Success';
export const LOAD_FAIL = '[Collection] Load Fail';


interface LoadPayload {
  pageSize: number;
  pageIndex: number;
  filterValue: string;
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = LOAD;
  constructor(public payload: LoadPayload ) {}
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: { entry: Patient[], total: number}) {}
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) {}
}

export type Actions =
  | Load
  | LoadSuccess
  | LoadFail;
