import {Action} from '@ngrx/store';
import {ExerciseInput} from 'mk-types';

export const SAVE_LOADING = '[Exercise] Save loading';
export const SAVE_SUCCES = '[Exercise] Save succes';
export const SAVE_ERROR = '[Exercise] Save error';

export class SaveLoadingExercise implements Action {
  readonly type = SAVE_LOADING;

  constructor(public payload: ExerciseInput) {}
}

export type Actions =  SaveLoadingExercise;
