import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ApolloQueryResult} from 'apollo-client';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import 'rxjs/add/observable/throw';
import 'rxjs/Observable';
const PatientsQuery = gql`
   query PatientsTableQuery($pageSize: Int, $pageIndex: Int, $filterValue: String) {
    getPatients(pageSize: $pageSize, pageIndex: $pageIndex, filterValue: $filterValue) {
       entry {
         id,
         name {
           family,
           given
         },
         email,
         phone,
         gender
       }
       total
     }
   }
 `;

@Injectable()
export class PatientService {

  constructor(private apollo: Apollo) {
  }
  getPatients({pageSize, pageIndex, filterValue}): Observable<ApolloQueryResult<any>> {
    console.log(filterValue);
    return this.apollo
      .watchQuery<any>({
        query: PatientsQuery,
        variables: {
          pageSize: pageSize,
          pageIndex: pageIndex,
          filterValue: filterValue
        }
      })
      .valueChanges
      .map(({ data }: ApolloQueryResult<any>) => data.getPatients)
      .catch(err => Observable.throw(err));
  }
}
