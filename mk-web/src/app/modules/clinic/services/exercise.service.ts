import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ApolloQueryResult} from 'apollo-client';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import 'rxjs/add/observable/throw';
import 'rxjs/Observable';
import {ExerciseInput} from 'mk-types';


@Injectable()
export class ExerciseService {

  constructor(private apollo: Apollo) {
  }

  save(exercise: ExerciseInput): Observable<ApolloQueryResult<ExerciseInput>> {
    console.log(exercise);
    return this.apollo
    .mutate({
      mutation: gql`
        mutation saveExercise($exercise: ExerciseInput) {
          saveExercise(exercise: $exercise) {
            name
          }
        }
      `,
      variables: {
        exercise
      },
    })
    .map(({data}: ApolloQueryResult<any>) => data.createExercise)
    .catch(err => Observable.throw(err));
  }
}
