import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {
  TablePatientPageContainer,
  ViewPatientPageContainer,
  AddPatientPageContainer,
  AddProgramPageContainer,
  AddExercisePageContainer
} from '../containers/';

const clinicRoutes: Routes = [
  {path: 'patient/search', component: TablePatientPageContainer},
  {path: 'patient/add', component: AddPatientPageContainer},
  {
    path: 'patient/:id',
    component: ViewPatientPageContainer,
  },
  {path: 'program/add', component: AddProgramPageContainer},
  {path: 'exercise/add', component: AddExercisePageContainer}
];

@NgModule({
  imports: [
    RouterModule.forChild(
      clinicRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class ClinicRoutingModule {
}
