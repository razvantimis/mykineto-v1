import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule, MatCardModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule, MatListModule, MatOptionModule, MatPaginatorModule, MatProgressBarModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule, MatTabsModule
} from '@angular/material';
import {StoreModule} from '@ngrx/store';
import {reducers} from '../reducers';
import {
  ViewPatientPageContainer,
  TablePatientPageContainer,
  EditPatientPageContainer,
  AddPatientPageContainer,
  AddProgramPageContainer,
  AddExercisePageContainer
} from '../containers/';
import {FormPatientComponent, FormProgramComponent} from '../components/';

import {ClinicRoutingModule} from './clinic-routing.module';
import {PatientEffects, ProgramEffects, ExerciseEffects} from '../effects';
import {PatientService, ProgramService, ExerciseService} from '../services';
import {FileUploadModule} from 'ng2-file-upload';
import {DndModule} from 'ng2-dnd';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    ClinicRoutingModule,
    StoreModule.forFeature('clinic', reducers),
    EffectsModule.forFeature([PatientEffects, ProgramEffects, ExerciseEffects]),
    FormsModule,
    ReactiveFormsModule,
    DndModule.forRoot(),
    FlexLayoutModule,
    FileUploadModule,
    MatFormFieldModule,
    MatTabsModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatProgressBarModule,
    MatListModule,
  ],
  providers: [PatientService, ProgramService, ExerciseService],
  declarations: [
    TablePatientPageContainer,
    ViewPatientPageContainer,
    EditPatientPageContainer,
    AddPatientPageContainer,
    AddProgramPageContainer,
    AddExercisePageContainer,
    FormPatientComponent,
    FormProgramComponent
  ]
})
export class ClinicModule {
}
