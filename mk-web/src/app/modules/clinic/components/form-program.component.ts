import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Program} from 'mk-types';


@Component({
  selector: 'mk-form-program',
  template: `
    <form [formGroup]="programForm" (ngSubmit)="onSaveProgram()">
      <mat-form-field>
        <input
          matInput
          formControlName="name"
          placeholder="Name"
          required/>
      </mat-form-field>
      <mat-form-field>
        <input
          matInput
          type="number"
          formControlName="frequencyNumber"
          placeholder="Number"
          required/>
      </mat-form-field>


      <mat-form-field>
        <mat-select placeholder="Select type" formControlName="frequencyType" required>
          <mat-option *ngFor="let item of codingFrequency" [value]="item.code">{{item.display}}</mat-option>
        </mat-select>
      </mat-form-field>

      <button type="submit" mat-raised-button color="primary">Save</button>
    </form>
  `,
  styles: ['']
})
export class FormProgramComponent implements OnInit {

  @Output() saveProgram: EventEmitter<Program> = new EventEmitter();
  programForm: FormGroup;
  codingFrequency: any[] = [
    {
      code: 'EVERYWEEK',
      display: 'EVERYWEEK'
    },
    {
      code: 'DAY',
      display: 'DAY'
    }
  ];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.programForm = this.fb.group({
      name: ['', Validators.required],
      frequencyNumber: ['', Validators.required],
      frequencyType: ['', Validators.required],
    });

  }

  onSaveProgram() {
    const program: Program = this.mapFormGroupToProgram();
    this.saveProgram.emit(program);
  }

  mapFormGroupToProgram(): Program {
    return <Program> {
      name: this.programForm.value.name,
      frequency: {
        number: this.programForm.value.frequencyNumber,
        type: this.programForm.value.frequencyType
      }
    };
  }
}
