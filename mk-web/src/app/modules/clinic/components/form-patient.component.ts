import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from 'mk-types';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'mk-form-patient',
  template: `
    <form [formGroup]="patientForm" (ngSubmit)="onSavePatient()">
      <mat-form-field>
        <input
          matInput
          formControlName="firstName"
          placeholder="First Name"
          required/>
      </mat-form-field>
      <mat-form-field>
        <input
          matInput
          formControlName="lastName"
          placeholder="Last Name"
          required/>
      </mat-form-field>
      <mat-form-field>
        <input
          matInput
          formControlName="phone"
          placeholder="Phone"
          required/>
      </mat-form-field>
      <mat-form-field>
        <input
          matInput
          formControlName="emailForm"
          placeholder="Email"/>
      </mat-form-field>
      <mat-form-field>
        <mat-select placeholder="Select gender" formControlName="gender" required>
          <mat-option value="male">M</mat-option>
          <mat-option value="female">F</mat-option>
        </mat-select>
      </mat-form-field>

      <button type="submit" mat-raised-button color="primary">Save</button>
    </form>
  `,
  styles: ['']
})
export class FormPatientComponent implements OnInit {

  @Input() disabled: Boolean = false;
  @Input() patientData: Patient;
  @Output() savePatient: EventEmitter<Patient> = new EventEmitter();

  patientForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.patientForm = this.fb.group({
      firstName: [{value: '', disabled: this.disabled}, Validators.required],
      lastName: [{value: '', disabled: this.disabled}, Validators.required],
      emailForm: [{value: '', disabled: this.disabled}, Validators.required],
      phone: [{value: '', disabled: this.disabled}, Validators.required],
      gender: [{value: '', disabled: this.disabled}, Validators.required],
    });

    if (this.patientData) {
      this.patientForm.setValue({
        firstName: this.patientData.name.given,
        lastName: this.patientData.name.family,
        emailForm: this.patientData.email,
        phone: this.patientData.phone,
        gender: this.patientData.gender
      });
    }
  }

  onSavePatient() {
    const patient: Patient = this.mapFormGroupToPatient();
    this.savePatient.emit(patient);
  }

  mapFormGroupToPatient(): Patient {
    return <Patient> {
      name: {
        family: this.patientForm.value.lastName,
        given: this.patientForm.value.firstName,
      },
      gender: this.patientForm.value.gender,
      phone: this.patientForm.value.phone,
      email: this.patientForm.value.emailForm,
      id: ''
    };
  }
}
