import {Component, OnInit, Input} from '@angular/core';
import {ToolbarHelpers} from './toolbar.helpers';

@Component({
  selector: 'mk-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() sidenav;
  @Input() sidebar;
  @Input() drawer;
  @Input() matDrawerShow;

  searchOpen: Boolean = false;
  toolbarHelpers = ToolbarHelpers;

  constructor() {
  }

  ngOnInit() {
  }

}
