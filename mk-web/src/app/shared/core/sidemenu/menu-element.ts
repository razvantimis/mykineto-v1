export const menus = [
  {
    'name': 'Dashboard',
    'icon': 'dashboard',
    'link': false,
    'open': false,
    'chip': {'value': 1, 'color': 'accent'},
    'sub': [
      {
        'name': 'Dashboard',
        'link': '/auth/dashboard',
        'icon': 'dashboard',
        'chip': false,
        'open': true,
      }

    ]
  },
  {
    'name': 'Patients',
    'icon': 'person',
    'link': false,
    'open': false,
    'sub': [
      {
        'name': 'Search patient',
        'link': '/clinic/patient/search/',
        'icon': 'search',
        'chip': false,
        'open': true,
      },
      {
        'name': 'Add patient',
        'link': '/clinic/patient/add/',
        'icon': 'add',
        'chip': false,
        'open': true,
      },
      {
        'name': 'Add program to patient',
        'link': '/clinic/patient/program/',
        'icon': 'schedule',
        'chip': false,
        'open': true,
      }

    ]
  },
  {
    'name': 'Programs',
    'icon': 'schedule',
    'link': false,
    'open': false,
    'sub': [
      {
        'name': 'Add program',
        'link': '/clinic/program/add/',
        'icon': 'add',
        'chip': false,
        'open': true,
      }
    ]
  },
  {
    'name': 'My Exercises',
    'icon': 'library_books',
    'link': '/clinic/exercise/add',
    'open': true
  }

];
